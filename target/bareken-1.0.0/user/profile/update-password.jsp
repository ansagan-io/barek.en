<%@ page import="com.epam.ans.app.entities.person.User" %>
<%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 20.02.21
  Time: 00:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <%String userName = (String) request.getAttribute("user-name");%>
    <title>Bareken: Changing <%=userName%>'s password</title>
    <%@include file="/header.jsp"%>
        <%
        Boolean passwordChanged = (Boolean) request.getAttribute("password-changed");

        %>

        <%if (passwordChanged == Boolean.TRUE) {%>
            <div class="container">
                <h1 class="h3 m-5 font-weight-normal text-center"> Dear "<%=userName%>", your password was successfully updated!</h1>
            </div>
        <%} else if (passwordChanged == Boolean.FALSE) {%>
            <div class="container">
                <h1 class="h3 m-5 font-weight-normal text-center"> Dear "<%=userName%>", you entered wrong password, please try again..</h1>
            </div>
        <%}%>

    <div class="container m-5 mx-auto" style="width: 500px;">
        <form class="justify-content-xxl-start" method="post" >
            <h1 class="h3 mb-3 font-weight-normal">Please fill in</h1>
            <div class="form-group">
                <label class="container-fluid" for="old-password">Old password
                    <input type="password" class="form-control" name="old-password"  id="old-password" placeholder="Please, enter your old password" >
                </label>

                <label class="container-fluid" for="new-password">New password
                    <input type="password" class="form-control" name="new-password" id="new-password" placeholder="Enter your email" >
                </label>

                <label class="container-fluid" for="new-password-repeat">Repeat new password
                    <input name="new-password-repeat" class="form-control" type="password" id="new-password-repeat" placeholder="Enter phone number">
                </label>
            </div>
            <input class="btn btn-primary" type="submit" value="Submit" onclick="return Validate()">
        </form>
    </div>

    <script >
        function Validate() {
            const password = document.getElementById("new-password").value;
            const confirmPassword = document.getElementById("new-password-repeat").value;
            if (password !== confirmPassword) {
                alert("Passwords do not match.");
                return false;
            }
            return true;
        }
    </script>
    <%@include file="/footer.jsp"%>
