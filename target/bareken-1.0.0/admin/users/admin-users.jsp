<%@ page import="java.util.Set" %>
<%@ page import="com.epam.ans.app.entities.person.User" %>
<%@ page import="com.epam.ans.app.model.DAO" %>
<%@ page import="com.epam.ans.app.entities.announcement.Contact" %><%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 03.03.21
  Time: 18:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
    <title>Admin: Users </title>
<%@include file="../header.jsp"%>
<%
    Set<User> users = DAO.getUserDAO().getAllUsers();
%>

<div class="container m-3">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">ID</th>
            <th scope="col">Username</th>
            <th scope="col">Create Date</th>
            <th scope="col">Gender</th>
            <th scope="col">Email</th>
            <th scope="col">Phone number</th>
            <th scope="col">Location</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        <%
            if (users.size() > 0) {
                long count = 1;
                for (User user : users) {
                    long userId = user.getId();
                    String userName = user.getUserName();
                    String createDate = user.getCreateDate().toString();
                    String gender = user.getGender().name();
                    Contact contact = user.getContact();
                    String email = contact.getEmail();
                    String phoneNumber = contact.getPhoneNumber();
                    String location = contact.getLocation();
        %>
        <tr>
            <th scope="row"><%=count%></th>
            <td><%=userId%></td>
            <td><%=userName%></td>
            <td><%=createDate%></td>
            <td><%=gender%></td>
            <td><%=email%></td>
            <td><%=phoneNumber%></td>
            <td><%=location%></td>
            <td>
                <a href="/admin/users/update-user?id=<%=userId%>">edit</a>
                <a href="/admin/users/delete-user?id=<%=userId%>">delete</a>
            </td>
        </tr>
        <%count++;
        }
        }%>
        </tbody>
    </table>
</div>
<%@include file="../footer.jsp"%>