<%@ page import="com.epam.ans.app.model.DAO" %>
<%@ page import="com.epam.ans.app.model.AnnouncementDAO" %>
<%@ page import="com.epam.ans.app.entities.announcement.TypeOfAnnouncement" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Admin: main page</title>
<%@include file="header.jsp"%>

<% Long usersCount = DAO.getUserDAO().getUsersCount();
AnnouncementDAO annDAO = DAO.getAnnouncementDAO();
Long announcementsCount = annDAO.getAnnouncementsCount();
Long sellingCount = annDAO.getAnnouncementsCount(TypeOfAnnouncement.SELLING);
Long buyingCount = annDAO.getAnnouncementsCount(TypeOfAnnouncement.BUYING);
Long forRentCount = annDAO.getAnnouncementsCount(TypeOfAnnouncement.FOR_RENT);
Long lostCount = annDAO.getAnnouncementsCount(TypeOfAnnouncement.LOST);
Long foundCount = annDAO.getAnnouncementsCount(TypeOfAnnouncement.FOUND);
Long questionsCount = DAO.getContactUsDAO().getContactUsCount();
%>

<div class="card container m-5" style="width: 40rem;">
    <div class="card-header">
        Statistics
    </div>
    <ul class="list-group list-group-flush">
        <li class="list-group-item">User's count: <%=usersCount%></li>
        <li class="list-group-item">Count of all announcements:  <%=announcementsCount%></li>
        <li class="list-group-item">Count of selling announcements: <%=sellingCount%></li>
        <li class="list-group-item">Count of buying announcements: <%=buyingCount%></li>
        <li class="list-group-item">Count of for rent announcements: <%=forRentCount%></li>
        <li class="list-group-item">Count of lost announcements: <%=lostCount%></li>
        <li class="list-group-item">Count of found announcements: <%=foundCount%></li>
        <li class="list-group-item">Count of questions: <%=questionsCount%></li>
    </ul>
</div>

<%@include file="footer.jsp"%>
