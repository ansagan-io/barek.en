<%@ page import="com.epam.ans.app.model.DAO" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.epam.ans.app.entities.announcement.ContactUs" %><%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 03.03.21
  Time: 20:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%
    Set<ContactUs> contactUsSet = DAO.getContactUsDAO().getQuestions();
%>
<html>
<head>
    <title>Admin: Questions</title>
    <%@include file="header.jsp" %>

    <div class="container m-5">
        <%if (contactUsSet.size() == 0) {%>
            <div class="w3-panel w3-green w3-display-container w3-card-4 w3-round">
                        <span onclick="this.parentElement.style.display='none'" class="w3-button w3-margin-right
                        w3-display-right w3-round-large w3-hover-green w3-border w3-border-green w3-hover-border-grey">x</span>
                <h3 class="h3 m-5 font-weight-normal text-center">
                    Sorry, currently there is still no announcements in review</h3>
            </div>
        <%} else {
            for (ContactUs contactUs : contactUsSet) {
                String title = contactUs.getQuestionTitle();
                String createDate = contactUs.getCreateDate().toString();
                String content = contactUs.getQuestionContent();
                String ownerName = contactUs.getOwnerName();
                String ownerEmail = contactUs.getOwnerEmail();%>
                <div class="card text-dark bg-light mb-3" style="max-width: 35rem">
                    <div class="card-header"><%=title%></div>
                    <div class="card-body">
                        <h5 class="card-title"><%=title%></h5>
                        <p class="card-text"><%=createDate%></p>
                        <p class="card-text"><%=content%></p>
                        <h5 class="card-title">Questioner name</h5>
                        <p class="card-text"> <%=ownerName%></p>
                        <h5 class="card-title">Questioner email</h5>
                        <p class="card-text"> <%=ownerEmail%></p>

                    </div>
                </div>
            <%}%>
        <%}%>
    </div>

    <%@include file="footer.jsp"%>
