<%@ page import="com.epam.ans.app.entities.announcement.Contact" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Bareken: Add new announcement </title>
    <%@include file="/header.jsp"%>
        <%String name ;
        Contact contact;
        String email;
        String phoneNumber;
        String location;
        String disabled;
        Long announcementId = (Long) request.getAttribute("announcement-id");
        String advertTitle = (String) request.getAttribute("announcement-title");
        if (announcementId !=null && advertTitle != null) {
        request.removeAttribute("user-id");
        request.removeAttribute("advert-id");%>
            <div class="container">
                <h1 class="h3 m-5 font-weight-normal text-center"> You have successfully submitted your
                    announcement <a href="${pageContext.request.contextPath}announcement?id=<%=announcementId%>">"<%=advertTitle%>"</a>
                    for review</h1>
            </div>
        <%} else if (currentUser != null) {
            name = currentUser.getUserName();
            contact = currentUser.getContact();
            email = contact.getEmail();
            phoneNumber = contact.getPhoneNumber();
            location = contact.getLocation();
            disabled = "disabled";%>
                <div class="container">
                    <h1 class="h3 m-3 text-center font-weight-normal" id="page-title">SELLING ANNOUNCEMENT</h1>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <form method="post">

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="type-of-announcement">
                                            Please choose the type of announcement that you are going to submit:
                                        </label>
                                    </div>
                                    <select class="custom-select" name="type-of-announcement" id="type-of-announcement">
                                        <option selected value="SELLING">Selling</option>
                                        <option value="BUYING">Buying</option>
                                        <option value="FOR_RENT">For rent</option>
                                        <option value="LOST">Lost</option>
                                        <option value="FOUND">Found</option>
                                    </select>
                                </div>

                                <div class="input-group mb-3" id="categories">
                                    <div class="input-group-prepend">
                                        <label class="input-group-text" for="category">Advertisement category: </label>
                                    </div>
                                    <select class="custom-select" name="category" id="category">
                                        <option selected value="NOT_DEFINED">-- select --</option>
                                        <option value="TRANSPORT">Transport</option>
                                        <option value="REALITY">Reality</option>
                                        <option value="ELECTRONICS">Electronics</option>
                                        <option value="HOUSE_AND_GARDEN">House and garden</option>
                                        <option value="JOB">Job</option>
                                        <option value="CLOTHING">Clothing</option>
                                        <option value="FOR_KIDS">For kids</option>
                                        <option value="SERVICES">Services</option>
                                        <option value="ANIMALS">Animals</option>
                                    </select>
                                </div>

                                <div class="row mb-3">
                                    <label for="announcement-title" class="col-sm-2 col-form-label">Title:</label>
                                    <div class="col-sm-10">
                                        <input type="text" name="announcement-title" class="form-control" id="announcement-title" required max="100">
                                    </div>
                                </div>


                                <div class="row mb-3">
                                    <label for="announcement-content" class="col-sm-2 col-form-label">Content of announcement: </label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="announcement-content" id="announcement-content" rows="5" required maxlength="500"></textarea>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label class="col-sm-2 col-form-label" for="value" id="value-label"> Price:</label>
                                    <div class="col-sm-1">
                                        <div class="input-group-text">KZT</div>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="number" name="value" class="form-control" id="value" required>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label for="owner-name" class="col-sm-2 col-form-label">Your name: </label>
                                    <div class="col-sm-10">
                                        <input type="text" name="owner-name" class="form-control" id="owner-name" value="<%=name%>" <%=disabled%>>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="email" class="col-sm-2 col-form-label">Contact email: </label>
                                    <div class="col-sm-10">
                                        <input type="email" name="email" class="form-control" id="email" value="<%=email%>" required max="45">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="phone-number" class="col-sm-2 col-form-label">Contact phone number: </label>
                                    <div class="col-sm-10">
                                        <input type="tel" name="phone-number" class="form-control" id="phone-number" value="<%=phoneNumber%>" required maxlength="45">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label for="location" class="col-sm-2 col-form-label">Location: </label>
                                    <div class="col-sm-10">
                                        <input type="text" name="location" class="form-control" id="location" value="<%=location%>" required maxlength="45">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit announcement</button>
                            </form>
                        </div>
                    </div>

                </div>
                <script>
                document.getElementById("new-ad-button").classList.add("active");
                let typeOfAnnouncement = document.getElementById("type-of-announcement");

                typeOfAnnouncement.addEventListener('change', (event) =>{
                    let value = event.target.value;
                    if (value === "LOST" || value === "FOUND") {
                        document.getElementById("categories").classList.add("d-none");
                        document.getElementById("value-label").textContent=("Award: ");
                    } else {
                        document.getElementById("categories").classList.remove("d-none");
                        document.getElementById("value-label").textContent=("Price: ");
                    }

                    switch (value) {
                        case "SELLING": document.getElementById("page-title").textContent=("SELLING ANNOUNCEMENT");
                            break;
                        case "BUYING": document.getElementById("page-title").textContent=("BUYING ANNOUNCEMENT");
                            break;
                        case "LOST": document.getElementById("page-title").textContent=("LOST ANNOUNCEMENT");
                            break;
                        case "FOUND": document.getElementById("page-title").textContent=("FOUND ANNOUNCEMENT");
                            break;
                        case "FOR_RENT": document.getElementById("page-title").textContent=("FOR RENT ANNOUNCEMENT");
                            break;
                    }
                })

            </script>
        <%} else {%>
            <h1 class="h3 m-5 font-weight-normal text-center"> You have to sign in first to add new Announcement</h1>
        <%}%>

<%@include file="/footer.jsp"%>

