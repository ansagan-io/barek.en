<%@ page import="com.epam.ans.app.model.AnnouncementDAO" %>
<%@ page import="com.epam.ans.app.entities.announcement.Announcement" %>
<%@ page import="com.epam.ans.app.entities.announcement.TypeOfAnnouncement" %>
<%@ page import="java.util.Set" %>
<%@ page import="com.epam.ans.app.model.DAO" %>
<%@ page import="com.epam.ans.app.entities.announcement.AnnouncementStatus" %>
<%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 17.01.21
  Time: 19:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%AnnouncementDAO announcementDAO = DAO.getAnnouncementDAO();
    String parameter = request.getParameter("type");
    TypeOfAnnouncement typeOfAnnouncement = null;
    try {
        typeOfAnnouncement = TypeOfAnnouncement.valueOf(parameter);
    } catch (IllegalArgumentException | NullPointerException e) {
        e.printStackTrace();
    }
    Set<Announcement> announcements;
    if (typeOfAnnouncement == null) {
        announcements = announcementDAO.getAnnouncements(AnnouncementStatus.ACTIVE);
        parameter = "";
    } else {
        announcements  = announcementDAO.getAnnouncements(AnnouncementStatus.ACTIVE, typeOfAnnouncement);
    }
    String titleName = typeOfAnnouncement == null ? "" : "\"" + typeOfAnnouncement.name() + "\"";
%>
<html>
<head>
    <title>Bareken: <%=titleName%> announcements</title>
    <%@include file="/header.jsp"%>

    <div class="container">
            <%if (announcements.size() == 0) {%>
                <div class="w3-panel w3-green w3-display-container w3-card-4 w3-round">
                    <span onclick="this.parentElement.style.display='none'" class="w3-button w3-margin-right
                    w3-display-right w3-round-large w3-hover-green w3-border w3-border-green w3-hover-border-grey">x</span>
                    <h3 class="h3 m-5 font-weight-normal text-center">
                        Sorry, currently there is still no active <%=parameter%> announcements!
                    </h3>
                </div>
            <%} else {
                for (Announcement advert : announcements) {
                    long id = advert.getId();
                    String title = advert.getTitle();
                    String create_date = advert.getCreateDate().toString();
                    double price = advert.getValue();%>
                    <div class="card text-dark bg-light mb-3" style="max-width: 35rem">
                        <div class="card-header">
                            <a href="${pageContext.request.contextPath}/announcements/announcement?id=<%=id%>">
                                <%=title%>
                            </a></div>
                        <div class="card-body">
                            <h5 class="card-title"><%=title%></h5>
                            <p class="card-text"><%=create_date%></p>
                            <h5 class="card-title">Price</h5>
                            <p class="card-text"><%=price%></p>
                            <%if (parameter!=null && !parameter.equals("LOST") && !parameter.equals("FOUND")) {
                                    String category = advert.getCategory().name();%>
                            <h5 class="card-title">Category</h5>
                            <p class="card-text"> <%=category%></p>
                            <%}%>
                        </div>
                    </div>
                    <%}
                    }%>
    </div>

    <%@include file="/footer.jsp"%>
