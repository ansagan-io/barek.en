<%@ page import="com.epam.ans.app.entities.person.User" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.sql.Timestamp" %>
<%@ page import="com.epam.ans.app.entities.person.Gender" %>
<%@ page import="com.epam.ans.app.entities.announcement.*" %>
<%--
  Created by IntelliJ IDEA.
  User: ansagan
  Date: 17.01.21
  Time: 20:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%
    User user = (User) session.getAttribute("current-user");
    String userName;
    String createDate;
    String gender;
    Contact contact;
    String phoneNumber;
    String location;
    String email;
    if (user == null) {
        response.sendRedirect("/user/sign-in");
    } else {
        userName = user.getUserName();
        createDate = user.getCreateDate().toString();
        gender = user.getGender() == Gender.F ? "female":"male";
        contact = user.getContact();
        email = contact.getEmail();
        phoneNumber = contact.getPhoneNumber();
        location = contact.getLocation();
    %>

<html>
<head>
    <title>Bareken: Profile of <%=userName%> </title>
<%@include file="/header.jsp"%>
            <div class="container m-3">
                <div class="card text-dark bg-light mb-3" style="max-width: 50rem;">
                    <div class="card-header"><%=userName%></div>
                    <div class="card-body">
                        <h5 class="card-title">Email</h5>
                        <p class="card-text"><%=email%></p>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Account created</h5>
                        <p class="card-text"><%=createDate%></p>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Gender</h5>
                        <p class="card-text"><%=gender%></p>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Phone number</h5>
                        <p class="card-text"><%=phoneNumber%></p>
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Location</h5>
                        <p class="card-text"><%=location%></p>
                    </div>
                </div>
                <div class="btn-group">
                    <a href="${pageContext.request.contextPath}profile/update-profile" class="btn btn-primary">Update profile information</a>
                    <a href="${pageContext.request.contextPath}profile/update-password" class="btn btn-primary">Change password</a>
                    <a href="${pageContext.request.contextPath}profile/delete-user" class="btn btn-primary">Delete account</a>
                    <a href="${pageContext.request.contextPath}profile/sign-out" class="btn btn-primary">Sign out</a>
                </div>
            </div>
            <h1 class="h3 m-5 font-weight-normal">All submitted announcements</h1>
            <div class="container m-3">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Type</th>
                        <th scope="col">Create Date</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%Set<Announcement> announcementSet = user.getAnnouncements();
                    if (announcementSet.size() > 0) {
                        long count = 1;
                        for (Announcement an:announcementSet) {
                            long anId = an.getId();
                            String title = an.getTitle();
                            TypeOfAnnouncement typeOfAnnouncement  = an.getTypeOfAnnouncement();
                            Timestamp anCreateDate = an.getCreateDate();
                            AnnouncementStatus announcementStatus = an.getAnnouncementStatus();%>
                            <tr>
                                <th scope="row"><%=count%></th>
                                <td><a href="/announcements/announcement?id=<%=anId%>"><%=title%></a></td>
                                <td><%=typeOfAnnouncement.name()%></td>
                                <td><%=anCreateDate.toString()%></td>
                                <td><%=announcementStatus.name()%></td>
                                <td>
                                    <a href="/announcements/update-announcement?id=<%=anId%>">edit</a>
                                    <%if (an.getAnnouncementStatus() == AnnouncementStatus.ACTIVE) {%>
                                    <a href="/announcements/deactivate-announcement?id=<%=anId%>">deactivate</a>
                                    <%} else if (an.getAnnouncementStatus() == AnnouncementStatus.DEACTIVATED) {%>
                                    <a href="/announcements/activate-announcement?id=<%=anId%>">activate</a>
                                    <%}%>
                                    <a href="/announcements/delete-announcement?id=<%=anId%>">delete</a>
                                </td>
                            </tr>
                        <%count++;
                        }
                    }%>
                    </tbody>
                </table>
            </div>
        <%}%>
<%@include file="/footer.jsp"%>