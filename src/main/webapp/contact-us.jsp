<%@ page import="com.epam.ans.app.entities.announcement.Contact" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Bareken: Ask us a question </title>
    <%@include file="/header.jsp"%>
        <%String name = "";
        Contact contact;
        String email = "";
        Boolean inserted = (Boolean) request.getAttribute("inserted");
        if (inserted != null && inserted.equals(Boolean.TRUE)) {%>
            <h1 class="h3 m-3 text-center font-weight-normal" id="answer-title">Your question was sent to our team and
                you will be contacted by the email address you sent us</h1>
        <%} else {
            if (currentUser != null) {
            name = currentUser.getUserName();
            contact = currentUser.getContact();
            email = contact.getEmail();
            }%>
            <div class="container">
                <h1 class="h3 m-3 text-center font-weight-normal" id="page-title">Please describe you question </h1>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <form method="post">
                            <div class="row mb-3">
                                <label for="question-title" class="col-sm-2 col-form-label">Question title:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="question-title" class="form-control" id="question-title" required max="100">
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="question-content" class="col-sm-2 col-form-label">Question: </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="question-content" id="question-content" rows="5" required maxlength="500"></textarea>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="owner-name" class="col-sm-2 col-form-label">Your name: </label>
                                <div class="col-sm-10">
                                    <input type="text" name="owner-name" class="form-control " id="owner-name" value="<%=name%>" >
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="email" class="col-sm-2 col-form-label">Contact email: </label>
                                <div class="col-sm-10">
                                    <input type="email" name="email" class="form-control" id="email" value="<%=email%>" required max="45">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit question</button>
                        </form>
                    </div>
                </div>

            </div>
    <%}%>
            <script>
                document.getElementById("contact-us-ad-button").classList.add("active");
            </script>
    <%@include file="/footer.jsp"%>

