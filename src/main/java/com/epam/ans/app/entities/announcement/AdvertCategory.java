package com.epam.ans.app.entities.announcement;

public enum AdvertCategory {
    TRANSPORT,
    REALITY,
    ELECTRONICS,
    HOUSE_AND_GARDEN,
    JOB,
    CLOTHING,
    FOR_KIDS,
    SERVICES,
    ANIMALS,
    NOT_DEFINED
}
