package com.epam.ans.app.entities.announcement;

public enum TypeOfAnnouncement {
    SELLING,
    BUYING,
    FOR_RENT,
    LOST,
    FOUND
}
