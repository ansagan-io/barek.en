package com.epam.ans.app.model;

import java.util.Random;
import java.util.Set;

public class IDGenerator {
    public static synchronized Long generateUserId() {
        Set<Long> userIDs = DAO.getUserDAO().getAllUsersId();
        Long newID;
        do {
            newID = Long.parseLong(generateRandomLong() + 0);
        } while (userIDs.contains(newID));

        return newID;
    }

    public static synchronized Long generateAnnouncementId() {
        Set<Long> ids = DAO.getAnnouncementDAO().getAllAnnouncementsId();
        long newID;
        do {
            newID = Long.parseLong(generateRandomLong() + 1);
        } while (ids.contains(newID));

        return newID;
    }

    public static synchronized Long generateContactId() {
        Set<Long> ids = DAO.getContactDAO().getAllContactsId();
        Long newID;
        do {
            newID = Long.parseLong(generateRandomLong() + 2);
        } while (ids.contains(newID));

        return newID;
    }

    public static synchronized Long generateContactUsId() {
        Set<Long> ids = DAO.getContactUsDAO().getAllContactUsId();
        Long newID;
        do {
            newID = Long.parseLong(generateRandomLong() + 2);
        } while (ids.contains(newID));

        return newID;
    }

    private static String generateRandomLong() {
        return String.valueOf(new Random().nextLong()).substring(3);
    }
}
