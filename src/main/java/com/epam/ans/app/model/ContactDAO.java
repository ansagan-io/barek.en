package com.epam.ans.app.model;

import com.epam.ans.app.entities.announcement.Contact;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;


public class ContactDAO {
    private final String CONTACT_TABLE = "contact";
    private final String CONTACT_ID = "id";
    private final String CONTACT_PHONE_NUMBER = "phone_number";
    private final String CONTACT_EMAIL = "email";
    private final String CONTACT_LOCATION = "location";
    private final String CONTACT_OWNER_ID = "owner_id";

    private final DatabaseHandler dbHandler = DatabaseHandler.getInstance();

    //createContact
    public boolean insertContact(Contact contact) {
        boolean inserted = false;
        String statement = String.format("insert into %s (%s, %s, %s, %s, %s) values (?, ?, ?, ?, ?);",
                CONTACT_TABLE, CONTACT_ID, CONTACT_EMAIL, CONTACT_PHONE_NUMBER, CONTACT_LOCATION, CONTACT_OWNER_ID);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement(statement)) {
            ps.setString(1, String.valueOf(contact.getId()));
            ps.setString(2, contact.getEmail());
            ps.setString(3, contact.getPhoneNumber());
            ps.setString(4, contact.getLocation());
            ps.setLong(5, contact.getOwnerId());
            inserted = ps.executeUpdate() > 0;
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return inserted;
    }

    //getContactById
    public Contact getContactByOwnerId(long id) {
        Contact contact = null;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select * from " +
                     CONTACT_TABLE + " where " + CONTACT_OWNER_ID + " = '" + id + "';")) {
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                String email = resultSet.getString(CONTACT_EMAIL);
                String phoneNumber = resultSet.getString(CONTACT_PHONE_NUMBER);
                String location = resultSet.getString(CONTACT_LOCATION);
                contact = new Contact(id, email, phoneNumber, location, id);
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return contact;
    }

    public Set<Long> getAllContactsId() {
        Set<Long> ids = null;
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement("select id from "+ CONTACT_TABLE +";")) {

            ResultSet resultSet = ps.executeQuery();
            ids = new HashSet<>();
            while (resultSet.next()) {
                ids.add(resultSet.getLong(CONTACT_ID));
            }
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }
        return ids;
    }

    //update contact
    public boolean updateContact(Contact contact) {
        boolean rowUpdated = false;
        String statement = String.format("update %s set %s = ?, %s = ?, %s = ? where %s = ?;",
                CONTACT_TABLE, CONTACT_EMAIL, CONTACT_PHONE_NUMBER, CONTACT_LOCATION, CONTACT_OWNER_ID);
        try (Connection connection = dbHandler.getDbConnection();
             PreparedStatement ps = connection.prepareStatement(statement)){
            ps.setString(1, contact.getEmail());
            ps.setString(2, contact.getPhoneNumber());
            ps.setString(3, contact.getLocation());
            ps.setLong(4, contact.getOwnerId());
            System.out.println(ps);
            rowUpdated = ps.executeUpdate() > 0;
        } catch (SQLException exception) {
            dbHandler.printSQLException(exception);
        }

        return rowUpdated;
    }
}
