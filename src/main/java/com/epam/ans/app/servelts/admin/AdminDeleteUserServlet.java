package com.epam.ans.app.servelts.admin;

import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet (value = "/admin/users/delete-user")
public class AdminDeleteUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("id");
        if (userId == null) {
            resp.sendRedirect("/admin/users");
        } else {
            long id;
            try {
                id = Long.parseLong(userId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
            User user = DAO.getUserDAO().getUserById(id);
            if (user == null) {
                throw new ServletException("There is no user with id " + id);
            } else {
                req.getRequestDispatcher("admin-delete-user.jsp").forward(req, resp);

            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String answer = req.getParameter("delete-user-answer");
        String userId = req.getParameter("user-id");
        if (answer.equals("YES")) {
            boolean deleteUser = DAO.getUserDAO().deleteUser(Long.parseLong(userId));
            if (deleteUser) {
                req.setAttribute("updated", Boolean.TRUE);
            } else {
                throw new ServletException("User was not deleted");
            }
        }
        resp.sendRedirect("/admin/users");
    }
}
