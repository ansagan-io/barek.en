package com.epam.ans.app.servelts;

import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.GlobalConstants;

import javax.servlet.http.HttpSession;

public class SessionUpdate {
    public static void updateUserInSession(HttpSession session, User user) {
        User currentUser = (User) session.getAttribute(GlobalConstants.CURRENT_USER);
         if (currentUser != null)
             session.setAttribute(GlobalConstants.CURRENT_USER, user);
    }
}
