package com.epam.ans.app.servelts.admin;

import com.epam.ans.app.entities.announcement.Contact;
import com.epam.ans.app.entities.person.Gender;
import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet (value = "/admin/users/update-user")
public class AdminUpdateUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userId = req.getParameter("id");
        if (userId == null) {
            resp.sendRedirect("/admin/users");
        } else {
            long id;
            try {
                id = Long.parseLong(userId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
            User user = DAO.getUserDAO().getUserById(id);
            if (user == null) {
                throw new ServletException("There is no user with id " + id);
            } else {
                req.setAttribute("user-update", user);
                req.getRequestDispatcher("admin-update-user.jsp").forward(req, resp);

            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("user-name");
        String email = req.getParameter("email");
        String phoneNumber = req.getParameter(("phone-number"));
        String location = req.getParameter("location");
        Gender gender = Gender.valueOf(req.getParameter("gender"));
        String userId = req.getParameter("user-id");
        User user = DAO.getUserDAO().getUserById(Long.parseLong(userId));
        user.setUserName(name);
        Contact contact = user.getContact();
        contact.setEmail(email);
        contact.setPhoneNumber(phoneNumber);
        contact.setLocation(location);
        user.setGender(gender);
        boolean updateUser = DAO.getUserDAO().updateUser(user);
        if (updateUser) {
            req.setAttribute("updated", Boolean.TRUE);
            doGet(req, resp);
        } else {
            throw new ServletException("User or contact was not updated");
        }
    }
}
