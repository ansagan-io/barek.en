package com.epam.ans.app.servelts.announcement;

import com.epam.ans.app.entities.announcement.*;
import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;
import com.epam.ans.app.model.GlobalConstants;
import com.epam.ans.app.model.IDGenerator;
import com.epam.ans.app.servelts.SessionUpdate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "add-announcement", value = "/announcements/add-announcement")
public class AddAnnouncement extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/announcements/add-announcement.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = (User) req.getSession().getAttribute(GlobalConstants.CURRENT_USER);
        if (currentUser != null) {
            TypeOfAnnouncement typeOfAnnouncement = TypeOfAnnouncement.valueOf(req.getParameter("type-of-announcement"));
            AdvertCategory category = AdvertCategory.valueOf(req.getParameter("category"));
            String title = req.getParameter("announcement-title");
            String content = req.getParameter("announcement-content");
            double value = Double.parseDouble(req.getParameter("value"));
            String email = req.getParameter("email");
            String phoneNumber = req.getParameter("phone-number");
            String location = req.getParameter("location");
            Long newAnnouncementId = IDGenerator.generateAnnouncementId();
            Contact contact = new Contact(IDGenerator.generateContactId(), email, phoneNumber, location, newAnnouncementId);
            Announcement announcement = new Announcement(newAnnouncementId, title, content,
                    currentUser.getId(), contact, value, category, typeOfAnnouncement, AnnouncementStatus.IN_REVIEW);
            boolean created = DAO.getAnnouncementDAO().createAnnouncement(announcement);
            if (created) {
                req.setAttribute("announcement-id", announcement.getId());
                req.setAttribute("announcement-title", announcement.getTitle());
                SessionUpdate.updateUserInSession(req.getSession(), DAO.getUserDAO().getUserById(currentUser.getId()));
                doGet(req, resp);
            } else {
                throw new ServletException("New announcement was not created");
            }
        } else {
            doGet(req, resp);
        }
    }
}
