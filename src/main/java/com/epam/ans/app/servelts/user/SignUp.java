package com.epam.ans.app.servelts.user;

import com.epam.ans.app.entities.announcement.Contact;
import com.epam.ans.app.entities.person.Gender;
import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;
import com.epam.ans.app.model.GlobalConstants;
import com.epam.ans.app.model.IDGenerator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;

@WebServlet(name = "sign-up", value = "/user/sign-up")
public class SignUp extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("sign-up.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = IDGenerator.generateUserId();
        String name = req.getParameter("user-name");
        String password = req.getParameter("password");
        String email = req.getParameter("email");
        String phoneNumber = req.getParameter(("phone-number"));
        String gender = req.getParameter("gender");
        String location = req.getParameter("location");
        Contact contact = new Contact(IDGenerator.generateContactId(), email, phoneNumber, location, id);
        User user = new User(id, name, password, Gender.valueOf(gender), contact);
        boolean userCreated = DAO.getUserDAO().insertUser(user);
        user.setCreateDate(new Timestamp(System.currentTimeMillis()));
        if (userCreated) {
            req.setAttribute("user-name", name);
            req.getSession().setAttribute(GlobalConstants.CURRENT_USER, user);
            doGet(req, resp);
        } else {
            throw new ServletException("user or contact was not created");
        }
    }
}
