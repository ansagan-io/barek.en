package com.epam.ans.app.servelts.user;

import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "sign-in", value = "/user/sign-in")
public class SignIn extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("sign-in.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("user-name");
        String password = req.getParameter("password");
        User user = DAO.getUserDAO().getUserByUserName(userName);

        if (user == null || !password.equals(user.getPassword())) {
            req.setAttribute("unsuccessful", "true");
            doGet(req, resp);
        } else {
            HttpSession session = req.getSession();
            session.setAttribute("current-user", user);
            resp.sendRedirect("/user/profile");
        }

    }
}
