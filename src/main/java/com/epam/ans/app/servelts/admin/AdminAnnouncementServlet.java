package com.epam.ans.app.servelts.admin;

import com.epam.ans.app.entities.announcement.Announcement;
import com.epam.ans.app.entities.announcement.AnnouncementStatus;
import com.epam.ans.app.model.DAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminAnnouncementServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String annId = req.getParameter("id");
        String action = req.getParameter("action");
        long id;
        Announcement announcement = null;
        if (annId == null) {
            resp.sendRedirect("/admin/announcements");
        } else {
            try {
                id = Long.parseLong(annId);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
            announcement = DAO.getAnnouncementDAO().getAnnouncementById(id);
            if (announcement == null) {
                throw new ServletException("There is no announcement with id " + id);
            }
        }

        if (action == null && announcement != null) {
            req.setAttribute("announcement-show", announcement);
            req.getRequestDispatcher("admin-ann-page.jsp").forward(req, resp);
        } else if (action!= null && action.equals("APPROVE") && announcement != null) {
            boolean changeStatus = DAO.getAnnouncementDAO().changeStatus(announcement.getId(), AnnouncementStatus.ACTIVE);
            if (changeStatus) {
                resp.sendRedirect("/admin/announcements");
            } else {
                throw new ServletException("Status of announcement " + announcement.getId() + " was not chaned");
            }
        } else if (action!= null && action.equals("REJECT") && announcement != null) {
            boolean changeStatus = DAO.getAnnouncementDAO().changeStatus(announcement.getId(), AnnouncementStatus.REJECTED);
            if (changeStatus) {
                resp.sendRedirect("/admin/announcements");
            } else {
                throw new ServletException("Status of announcement " + announcement.getId() + " was not chaned");
            }
        } else if (action!= null && action.equals("DELETE") && announcement != null) {
            boolean changeStatus = DAO.getAnnouncementDAO().deleteAnnouncementById(announcement.getId());
            if (changeStatus) {
                resp.sendRedirect("/admin/announcements");
            } else {
                throw new ServletException("Status of announcement " + announcement.getId() + " was not chaned");
            }
        }
    }
}
