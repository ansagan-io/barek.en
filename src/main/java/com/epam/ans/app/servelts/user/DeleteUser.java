package com.epam.ans.app.servelts.user;

import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "delete-user", value = "/user/profile/delete-user")
public class DeleteUser extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("current-user");
        if (user == null) {
            resp.sendRedirect("/user/sign-in");
        } else {
            req.setAttribute("user-name", user.getUserName());
            req.getRequestDispatcher("delete-user.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String answer = req.getParameter("answer");
        if (answer.equals("YES")) {
            boolean success = DAO.getUserDAO().deleteUser(((User) req.getSession().getAttribute("current-user")).getId());
            if (success) {
                req.getSession().removeAttribute("current-user");
                resp.sendRedirect("/user/sign-in");
            }
        } else {
            resp.sendRedirect("/user/profile");
        }
    }
}
