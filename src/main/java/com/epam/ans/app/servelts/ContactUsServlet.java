package com.epam.ans.app.servelts;

import com.epam.ans.app.entities.announcement.*;
import com.epam.ans.app.model.DAO;
import com.epam.ans.app.model.IDGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "contact-us", value = "/contact-us")
public class ContactUsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/contact-us.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("question-title");
        String content = req.getParameter("question-content");
        String name = req.getParameter("owner-name");
        String email = req.getParameter("email");
        ContactUs contactUs;
        contactUs = new ContactUs(IDGenerator.generateContactUsId(), title, content, name, email);
        boolean created = DAO.getContactUsDAO().insertContactUs(contactUs);
        if (created) {
            req.setAttribute("inserted", Boolean.TRUE);
            doGet(req, resp);
        } else {
            throw new ServletException("Contact Us question was not created");
        }
    }
}
