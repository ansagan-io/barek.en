package com.epam.ans.app.servelts.user;

import com.epam.ans.app.entities.person.User;
import com.epam.ans.app.model.DAO;
import com.epam.ans.app.model.GlobalConstants;
import com.epam.ans.app.servelts.SessionUpdate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "update-password", value = "/user/profile/update-password")
public class UpdatePassword  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("current-user");
        if (user == null) {
            resp.sendRedirect("/user/sign-in");
        } else {
            req.setAttribute("user-name", user.getUserName());
            req.getRequestDispatcher("update-password.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User currentUser = (User) req.getSession().getAttribute(GlobalConstants.CURRENT_USER);
        String oldPassword = req.getParameter("old-password");
        String newPassword = req.getParameter("new-password");
        String newPasswordRepeat = req.getParameter(("new-password-repeat"));
        if (currentUser != null) {
            if (currentUser.getPassword().equals(oldPassword) && newPassword.equals(newPasswordRepeat)) {
                currentUser.setPassword(newPassword);
                boolean success = DAO.getUserDAO().updateUserPassword(currentUser.getId(), newPassword);
                if (success) {
                    SessionUpdate.updateUserInSession(req.getSession(), DAO.getUserDAO().getUserById(currentUser.getId()));
                    req.setAttribute("password-changed", Boolean.TRUE);
                } else {
                    req.setAttribute("password-changed", Boolean.FALSE);
                }
            } else {
                req.setAttribute("password-changed", Boolean.FALSE);
            }
            doGet(req, resp);
        }


    }
}
